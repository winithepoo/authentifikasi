<?php
class Customers_model extends CI_Model {

    var $CompanyName  = '';
    var $ContactName = '';
	var $Foto = '';
    

    function __construct()
    {        
        parent::__construct();
    }
    
    function get_customers()
    {
        $query = $this->db->get('customers');
        return $query->result();
    }

    function get_customers_by_id($id)
    {
        $this->db->where('CustomerID',$id);
        $query = $this->db->get('customers');
        return $query->row();
    }

    function insert_entry()
    {
        $this->CompanyName  = $this->input->post('CompanyName',true); 
		$this->ContactName  = $this->input->post('ContactName',true); 
	    if( ! empty($_FILES['userfile']['tmp_name'])) 
		{
		   $this->do_upload();
		   $upload_data  = $this->upload->data();                      
		   $this->Foto   = $upload_data['file_name'];                
		}		
        
		return $this->db->insert('customers', $this);
    }

    function update_entry()
    {
        $this->CompanyName  = $this->input->post('CompanyName',true); 
        $this->ContactName   = $this->input->post('ContactName',true);
		//echo $_FILES['userfile']['tmp_name'];
		//exit;
	    if( ! empty($_FILES['userfile']['tmp_name'])) 
		{
		   $this->do_upload();
		   $upload_data  = $this->upload->data();                      
		   $this->Foto   = $upload_data['file_name'];                
		}			
        return $this->db->update('customers', $this, array('CustomerID' => $this->input->post('id',true)));
    }

    function hapus($id)
    {
        $this->db->where('CustomerID',$id);
        return $this->db->delete('customers');
    }

    function cek_dependensi($id)
    {
        $this->db->where('CustomerID',$id);
        $query = $this->db->count_all('customers');
        return ($query==0) ? true : false;
    }
	
	function do_upload()
    {
        if( ! empty($_FILES['userfile']['tmp_name']))
        {     
			$config['encrypt_name']  = TRUE;
            $config['upload_path']   = FCPATH.'upload/';
    		$config['allowed_types'] = 'jpg|gif|png|bmp';
    		$config['max_size']      = '1000';
    		$config['max_width']     = '2024';
    		$config['max_height']    = '1468';
    		$this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload())
            {
				echo $this->upload->display_errors();
                exit;
                return FALSE;
            } 
            else
            {       
                $this->load->library('image_lib');
				$upload_data   = $this->upload->data();
				
				//watermark
				$config_watermark = array(); 
				$config_watermark['source_image']  = $upload_data ['full_path'];	
				$config_watermark['image_library'] = 'gd2';				
				$config_watermark['wm_text'] 	   = '12131276-Parisaktiana';
				$config_watermark['wm_type'] 	   = 'text';
				$config_watermark['wm_font_path']  = './system/fonts/texb.ttf';
				$config_watermark['wm_font_size']  = '16';
				$config_watermark['wm_font_color'] = '#707A7C';
				$this->image_lib->initialize($config_watermark);
				if (!$this->image_lib->watermark()) {
					echo $this->image_lib->display_errors();
                    exit;
					return FALSE;
				}
				
				//resize
				$width  = 600;
                $height = ($width / $upload_data['image_width']) * $upload_data['image_height'];          
                $config_resize = array('source_image' => $upload_data ['full_path'],
                       'image_library'   => 'gd2',
    				   'new_image'       => FCPATH.'upload/thumb/',
                       'create_thumb'    => TRUE,
    				   'maintain_ration' => TRUE,
    				   'width'           => $width,
    				   'height'          => $height);
				$this->image_lib->initialize($config_resize);		
                
                if( ! $this->image_lib->resize())
                {
                   	echo $this->image_lib->display_errors();
                    exit;
                    return FALSE;
                }
				
            }            
            return TRUE;
        } 
    }
}